package ru.maksimvoloshin.mytasks.locks;

import ru.maksimvoloshin.mytasks.handler.EventHandler;
import ru.maksimvoloshin.mytasks.handler.EventHandlerImpl;
import ru.maksimvoloshin.mytasks.handler.MyEvent;

public class Main {
    public static void main(String[] args) throws Exception {
        EventHandler eventHandler = new EventHandlerImpl();
        System.out.println("Start");
        eventHandler.startHandler();
        System.out.println("Add");
        for(int i = 0; i < 100; i++) {
            eventHandler.addEvent(new MyEvent(Thread.currentThread().getName() + " " + i));
        }
        System.out.println("Stop");
        Thread.sleep(1000);
        System.out.println("Add new 10");
        for(int i = 0; i < 10; i++) {
            eventHandler.addEvent(new MyEvent(Thread.currentThread().getName() + " 2 " + i));
        }
        System.out.println("Start");
        eventHandler.startHandler();
        System.out.println("Wait 10");
        Thread.sleep(15000);
        System.out.println("Stop");
        eventHandler.stopHandler();
    }
}
