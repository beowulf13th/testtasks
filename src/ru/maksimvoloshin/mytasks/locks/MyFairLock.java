package ru.maksimvoloshin.mytasks.locks;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;

public class MyFairLock {
    private volatile AtomicReference<Thread> currentThread = new AtomicReference<>();

    private volatile int deeper = 0;
    private final Queue<Object> threadQueueObjects = new ArrayBlockingQueue<>(101);

    public void lock() throws InterruptedException {
        if (currentThread.compareAndSet(null, Thread.currentThread())) {
            deeper = 1;
            return;
        }
        if (currentThread.get() == Thread.currentThread()) {
            deeper++;
            return;
        }
        if (currentThread.get() != Thread.currentThread()) {
            Object lockObject = new Object();
            threadQueueObjects.add(lockObject);
            synchronized (lockObject) {
                while (currentThread.get() != Thread.currentThread()) {
                    lockObject.wait();
                }
            }
        }
        deeper = 1;
    }

    public void unlock() {
        if (currentThread.get() != Thread.currentThread()) {
            throw new IllegalThreadStateException("Lock was not caught");
        }
        deeper--;
        Object lockObject = threadQueueObjects.poll();

        if(lockObject != null) {
            synchronized (lockObject) {
                if (deeper == 0) {
                    currentThread.set(Thread.currentThread());
                }
                lockObject.notifyAll();
            }
        }
    }
}