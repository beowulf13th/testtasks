package ru.maksimvoloshin.mytasks.locks;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author <a href="mailto:me@maksimvoloshin.ru">Maksim Voloshin</a>
 * @since 0.1
 */
public class MySecondFairLock {
    private Map<Thread, Object> lockMap = new HashMap<>();
    private AtomicReference<Thread> currentThread = new AtomicReference<>();

    private volatile Queue<Thread> lockedThreads = new LinkedList<>();

    private Object queueLock = new Object();
    private volatile Thread threadToWakeUp;

    private volatile int deeper = 0;

    public void lock() throws InterruptedException {
        Object lockObject;
        synchronized (queueLock) {
            if (currentThread.compareAndSet(null, Thread.currentThread())) {
                // todo first time
                deeper = 1;
                return;
            }
            if (currentThread.get() == Thread.currentThread()) {
                // todo reentrant
                deeper++;
                return;
            }
            // todo need to lock
            lockObject = new Object();
            lockMap.put(Thread.currentThread(), lockObject);
            lockedThreads.offer(Thread.currentThread());
            System.out.println("Placing " + Thread.currentThread().getName() + " into queue");
        }

        synchronized (lockObject) {
            while (threadToWakeUp != Thread.currentThread()) {
                lockObject.wait();
            }
            currentThread.set(Thread.currentThread());
        }
        deeper = 1;
    }

    public void unlock() {
        if(currentThread.get() != Thread.currentThread()) {
            throw new IllegalThreadStateException("O_O");
        }

        synchronized (queueLock) {
            deeper--;
            if (deeper != 0) {
                return;
            }

            Thread nextThread = lockedThreads.poll();
            if (nextThread == null) {
                currentThread.set(null);
                return;
            }
            Object lockObject = lockMap.get(nextThread);
            lockMap.remove(nextThread);
            synchronized (lockObject) {
                threadToWakeUp = nextThread;
                lockObject.notifyAll();
            }
        }
    }
}