package ru.maksimvoloshin.mytasks.locks;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

public class MyLockUser {
    private static CountDownLatch countDownLatch = new CountDownLatch(100);

    public static void main(String[] args) {
        final Test test = new Test(countDownLatch);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    test.print();
                } catch (InterruptedException e) {
                }
            }
        };

        for (int i = 0; i < 100; i++) {
            new Thread(r).start();
            countDownLatch.countDown();
        }
    }
}

class Test {
    private MySecondFairLock lock = new MySecondFairLock();
    private CountDownLatch countDownLatch;

    private AtomicBoolean flag = new AtomicBoolean(false);

    public Test(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public void print() throws InterruptedException {
        countDownLatch.await();
        lock.lock();
        System.out.println("In thread " + Thread.currentThread().getName());
        if(!flag.compareAndSet(false, true)) {
            throw new IllegalMonitorStateException("In thread " + Thread.currentThread().getName());
        }
        Thread.sleep(1000);
        flag.compareAndSet(true, false);
        System.out.println("Thread finished " + Thread.currentThread().getName());
        lock.unlock();
    }
}