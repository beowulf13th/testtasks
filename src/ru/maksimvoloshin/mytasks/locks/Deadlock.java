package ru.maksimvoloshin.mytasks.locks;

import java.util.concurrent.CountDownLatch;

public class Deadlock {
    public Integer a = new Integer(0);
    public Integer b = new Integer(0);

    public static void main(String[] args) throws InterruptedException {
        Deadlock deadlock = new Deadlock();
        CountDownLatch countDownLatch = new CountDownLatch(2);

        A a = new A(deadlock, countDownLatch);
        B b = new B(deadlock, countDownLatch);

        a.start();
        b.start();
    }
}

class A extends Thread {
    private final CountDownLatch cdl;
    Deadlock deadlock;

    public A(Deadlock deadlock, CountDownLatch countDownLatch) {
        this.deadlock = deadlock;
        this.cdl = countDownLatch;
    }

    @Override
    public void run() {
        synchronized (deadlock.a) {
            try {
                cdl.countDown();
                cdl.await();
            } catch (InterruptedException e) {
            }
            synchronized (deadlock.b) {
                System.out.println("In a and b");
            }
        }
    }
}

class B extends Thread {
    private final CountDownLatch cdl;
    Deadlock deadlock;

    public B(Deadlock deadlock, CountDownLatch countDownLatch) {
        this.deadlock = deadlock;
        this.cdl = countDownLatch;
    }

    @Override
    public void run() {
        synchronized (deadlock.b) {
            try {
                cdl.countDown();
                cdl.await();
            } catch (InterruptedException e) {
            }
            synchronized (deadlock.a) {
                System.out.println("In b and a");
            }
        }
    }
}