package ru.maksimvoloshin.mytasks.locks;

import java.util.concurrent.atomic.AtomicReference;

public class MySpinLock {
    private AtomicReference<Thread> currentThread = new AtomicReference<>();
    private int deeper = 0;

    public void lock() throws InterruptedException {
        if (currentThread.get() == Thread.currentThread()) {
            deeper++;
            return;
        }
        while (!currentThread.compareAndSet(null, Thread.currentThread())) {
            // wait
        }
        deeper++;
    }

    public void unlock() {
        if(currentThread.get() != Thread.currentThread()) {
            throw new IllegalThreadStateException("Lock not catched!");
        }
        deeper--;
        if(deeper == 0)
            currentThread.set(null);
    }
}