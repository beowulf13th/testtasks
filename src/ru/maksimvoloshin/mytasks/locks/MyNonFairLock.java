package ru.maksimvoloshin.mytasks.locks;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyNonFairLock {
    private Thread currentThread;
    private int deeper = 0;

    public synchronized void lock() throws InterruptedException {
        while (currentThread != null && currentThread != Thread.currentThread()) {
            wait();
        }
        deeper++;
        currentThread = Thread.currentThread();
    }

    public synchronized void unlock() {
        if(currentThread != Thread.currentThread()) {
            throw new IllegalThreadStateException("Lock not catched!");
        }
        deeper--;
        if(deeper == 0)
            currentThread = null;

        notifyAll();
    }
}