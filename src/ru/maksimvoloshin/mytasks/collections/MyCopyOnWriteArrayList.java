package ru.maksimvoloshin.mytasks.collections;

import java.util.Iterator;

public class MyCopyOnWriteArrayList<T> implements Iterable<T> {
    private Object[] array;
    private int capacity = 8;
    private int cursor = 0;

    MyCopyOnWriteArrayList() {
        array = new Object[capacity];
    }

    public void add(T elem) {
        cursor++;
        checkAndAddCapacity();
        array[cursor] = elem;
    }

    public T get(int index) {
        return (T) array[index];
    }

    private void checkAndAddCapacity() {
        if(cursor >= capacity) {
            capacity *= 2;
            Object[] oldArray = array.clone();
            array = new Object[capacity];
            System.arraycopy(oldArray, 0, array, 0, oldArray.length);
        }
    }

    public Iterator<T> iterator() {
        return new Itr(array);
    }

    private class Itr implements Iterator<T> {
        private int cursor = 0; //
        private Object[] data;

        private Itr(Object[] array) {
            data = array;
        }

        @Override
        public T next() {
            cursor++;
            return (T) data[cursor];
        }

        @Override
        public boolean hasNext() {
            return cursor < array.length && data[cursor+1] != null;
        }

        @Override
        public void remove() {

        }
    }
}