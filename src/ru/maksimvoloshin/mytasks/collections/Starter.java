package ru.maksimvoloshin.mytasks.collections;

import java.util.Iterator;

public class Starter {
    public static void main(String[] args) {
        MyCopyOnWriteArrayList<String> strings = new MyCopyOnWriteArrayList<>();
        strings.add("asdfasdf1");
        strings.add("asdfasdfsf2");
        strings.add("asdfasdf3");
        strings.add("asdfasdfsf4");
        strings.add("asdfasdf5");
        strings.add("asdfasdfsf6");
        strings.add("asdfasdf7");
        strings.add("asdfasdfsf8");
        strings.add("asdfasdf9");
        strings.add("asdfasdfsf10");

        Iterator iter = strings.iterator();
        while(iter.hasNext()) {
            System.out.println(String.valueOf(iter.next()));
        }
    }
}