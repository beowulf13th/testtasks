package ru.maksimvoloshin.mytasks.collections;

public interface MyIterator<T> {
    T next();
    boolean hasNext();
    void remove();
}