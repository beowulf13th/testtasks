package ru.maksimvoloshin.mytasks.generics;

public class GenericsCell<E> {
    E value;

    GenericsCell (E v) {
        value = v;
    }

    E get() {
        return value;
    }

    void set(E v) {
        value = v;
    }

    public static void main(String[] args) {
        GenericsCell x = new GenericsCell<String>("abs");
        System.out.println(x.value);
        System.out.println(x.get());
        x.set("asdfsd");
    }
}