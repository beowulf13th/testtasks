package ru.maksimvoloshin.mytasks.handler;

import java.util.ArrayList;

/**
 * @REVIEW �� � ����� ������ - ��� �������, ��� �����
 */
public class EventHandlerReview {

    public static class EventDispatcher {
        private static EventDispatcher instance;
        private ArrayList<EventListener> eventListeners = new ArrayList<EventListener>();

        // @REVIEW ���� ��������, �� ��� ���������� ������������
        // ������� ��������������� ������
        // ����� double check locking + volatile ����������
        public static EventDispatcher getInstance() {
            if (instance == null) {
                // @REVIEW ��� ��� instance ����� ���� ������������������ ������ �����
                synchronized (EventDispatcher.class) {
                    instance = new EventDispatcher();
                }
            }
            return instance;
        }

        // @REVIEW �� thread-safe
        public void putEvent(Event event) {
            for (EventListener listener : eventListeners) {
                listener.onEvent(event);
            }
        }

        // @REVIEW �� thread safe
        public void addListener(EventListener eventListener) {
            eventListeners.add(eventListener);
        }

    }

    interface EventListener {
        void onEvent(Event event);
    }

    public static class Event {
        private Object payload;
        public Event(Object payload) {
            this.payload = payload;
        }
        public Object getPayload() {
            return payload;
        }
    }

    public static class EventPusher extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                EventDispatcher.getInstance().putEvent(new Event(new Object()));
            }
        }
    }

    public static class EventListenerRegistrator extends Thread {
        @Override
        public void run() {
            EventDispatcher.getInstance().addListener(new EventListener() {
                @Override
                public void onEvent(Event event) {
                    System.out.println("Event: " + event);
                }
            });
        }
    }

    public static void main(String[] args) {
        new EventListenerRegistrator().start();
        new EventPusher().start();
    }
}
