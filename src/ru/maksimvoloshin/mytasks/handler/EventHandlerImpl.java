package ru.maksimvoloshin.mytasks.handler;

import java.util.LinkedList;

public class EventHandlerImpl implements EventHandler {
    private LinkedList<MyEvent> events = new LinkedList<MyEvent>();
    private EventHandlerWorker eventHandlerWorker = new EventHandlerWorker();

    public EventHandlerImpl() {
        System.out.println("EventHandler created");
    }

    public void addEvent(MyEvent event) throws Exception {
        System.out.println("Add event");
        synchronized (events) {
            events.add(event);
            events.notifyAll();
        }
    }

    public void startHandler() throws Exception {
        System.out.println("Start executor");
        if(stopped) {
            stopped = false;
            System.out.println("Try to start thread  " + eventHandlerWorker.getState());
            eventHandlerWorker = new EventHandlerWorker();
            eventHandlerWorker.start();
        }
    }

    public void stopHandler() throws Exception {
        System.out.println("Stop executor");
        stopped = true;
        synchronized (events) {
            events.notifyAll();
        }
        System.out.println("Waiting for event thread stop " + eventHandlerWorker.getState());
        eventHandlerWorker.join();
        System.out.println("Event thread stopped");
    }

    private boolean stopped = true;

    private class EventHandlerWorker extends Thread {
        @Override
        public void run() {
            while(!stopped) {
                MyEvent event = null;
                synchronized (events) {
                    while(true && !stopped) {
                        event = events.poll();
                        if (event == null) {
                            try {
                                events.notifyAll();
                                events.wait();
                                continue;
                            } catch (InterruptedException e) {
                                System.out.println(e.getMessage());
                            }
                        }
                        break;
                    }
                }

                if(event != null) {
                    try {
                        event.execute();
                    } catch (InterruptedException e) {
                        System.out.println("execute failed " + e.getMessage());
                    }
                }
            }
            System.out.println("Leave run");
        }
    }
}