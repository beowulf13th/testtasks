package ru.maksimvoloshin.mytasks.sorts;

import java.util.Arrays;

/**
 * @author <a href="mailto:me@maksimvoloshin.ru">Maksim Voloshin</a>
 * @since 0.1
 */
public class InsertionSort {
    private static int arraySize = 10;

    public static void main(String[] args) {
        int[] array = new int[arraySize];
        // set
        for(int i = 0; i < arraySize; i++) {
            array[i] = (int) (Math.random()*100);
        }
        System.out.println(Arrays.toString(array));
        // sort

        System.out.println(Arrays.toString(array));
        // reverse sort

        System.out.println(Arrays.toString(array));
    }

    public static void insertionSort(int[] array) {
        for(int j = 1; j < arraySize; j++) {
            int key = array[j];
            int i = j-1;
            while(i >= 0 && array[i] > key) {
                array[i+1] = array[i];
                i--;
            }
            array[i+1] = key;
        }
    }

    public static void insertionReverseSort(int[] array) {
        for(int j = 1; j < arraySize; j++) {
            int key = array[j];
            int i = j-1;
            while(i >= 0 && array[i] < key) {
                array[i+1] = array[i];
                i--;
            }
            array[i+1] = key;
        }
    }
}